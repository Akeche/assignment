package akeche.com.assignment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button first, second , third;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        first = (Button) findViewById(R.id.first_screen);
        first.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =  new Intent(MainActivity.this, ImageActivity.class);
                intent.putExtra("image", R.drawable.akeche);
                startActivity(intent);
            }
        });
        second = (Button) findViewById(R.id.second_screen);
        second.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =  new Intent(MainActivity.this, ImageActivity.class);
                intent.putExtra("image", R.drawable.akeche3);
                startActivity(intent);

            }
        });
        third = (Button) findViewById(R.id.third);
        third.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =  new Intent(MainActivity.this, ImageActivity.class);
                intent.putExtra("image", R.drawable.akeche4);
                startActivity(intent);
            }
        });
    }
}
